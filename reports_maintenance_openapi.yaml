openapi: 3.0.0
info:
  version: 1.0.0
  title: Отчеты по плановым ремонтным работам

security:
  - bearerAuth: []

paths:
  '/api/reports/maintenance/{object_id}':
    get:
      summary: Получение списка плановых ремонтных работ (ПРР) на объекте и ближайших объектах с отчетами по ним
      parameters:
        - $ref: '#/components/parameters/object_id'
        - $ref: '#/components/parameters/date1'
        - $ref: '#/components/parameters/date2'
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/MaintenanceReportData'
        '401':
          $ref: '#/components/responses/ResponseError401'
        '403':
          $ref: '#/components/responses/ResponseError403'
        '404':
          $ref: '#/components/responses/ResponseError404'
        '500':
          $ref: '#/components/responses/ResponseError500'


components:

  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT

  parameters:
    date1:
      in: query
      name: uc
      required: true
      description: Дата начала интевала
      schema:
        type: string
        example: "2022-02-01"
    date2:
      in: query
      name: uc
      required: true
      description: Дата окончания интевала
      schema:
        type: string
        example: "2022-12-01"
    object_id:
      in: path
      name: object_id
      required: true
      description: Идентификатор объекта
      schema:
        type: integer

  responses:
  
    ResponseError400:
      description: Ответ в случае ошибки 400
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorData'
          examples:
            400:
              value: 
                {"errors": [{"message": "Недопустимое значение для поля \"Комментарий специалиста\"", "type": "wrongValue", "field": "comment"}]}
    ResponseError401:
      description: Ответ в случае ошибки 401
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorData'
          examples:
            401:
              value: 
                {"errors": [{"message": "Неверное имя или пароль"}]}
    ResponseError403:
      description: Ответ в случае ошибки 403
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorData'
          examples:
            403:
              value: 
                {"errors": [{"message": "Недостаточно прав доступа"}]}
    ResponseError404:
      description: Ответ в случае ошибки 404
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorData'
          examples:
            404:
              value: 
                {"errors": [{"message": "Объект не найден"}]}
    ResponseError500:
      description: Ответ в случае ошибки 500
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorData'
          examples:
            500:
              value: 
                {"errors": [{"message": "Неизвестная ошибка"}]}

  schemas:

    ErrorData:
      type: object
      properties:
        errors:
          type: array
          items:
            type: object
            properties:
              message:
                description: Сообщение об ошибке для пользователя
                type: string
              type:
                type: string
                description: Идентификатор ошибки
                enum:
                  - missingField
                  - wrongValue
              field:
                type: string
                description: Идентификатор поля, которое содержит ошибочное значение
    

    MaintenanceReportData:
      description: Элемент списка (ПРР + объект + отчеты)
      type: object
      properties:
        group:
          description: Группа ПРР - прнадлежность к выбранному объекту, next- или prev- объектам
          type: string
          enum: ['next', 'prev', 'current']
          example: 'prev'
        object:
          $ref: '#/components/schemas/ObjectReportData'
        actions:
          description: Массив ремонтов на объекте
          type: array
          items:
            $ref: '#/components/schemas/ActionMaintenanceData'

    ActionMaintenanceData:
      description: Информация про ПРР для отчетов
      type: object
      properties:
        id:
          description: Идентификатор ПРР
          type: integer
          example: 44
        version:
          description: Версия ПРР
          type: integer
          example: 345353543
        name:
          description: Наименование ПРР
          type: string
          example: 'Реконструкция водозабора'
        startDate:
          description: Дата планового начала инвестмероприятия
          type: string
          format: date-time
          example: "2021-02-09T12:00:00Z"
        endDate:
          description: Дата планового завершения инвестмероприятия
          type: string
          format: date-time
          example: "2022-02-09T12:00:00Z"
        reports:
          $ref: '#/components/schemas/ReportsData'


    ObjectReportData:
      description: Информация про объект для отчетов
      type: object
      properties:
        id:
          description: Идентификатор связанного с инвестмероприятем объекта
          type: integer
          example: 34556
        version:
          description: Версия объекта
          type: integer
          example: 1234523535
        type:
          description: Тип объекта
          type: string
          enum: ['WATER_WELL', 'CLEAN_WATER_TANK', 'PUMP', 'WATER_TOWER', 'WATER_QUARTER_NET']
          example: 'WATER_WELL'
        systemType:
          description: Система объекта
          type: string
          enum: ['COLD', 'HOT', 'SEW']
          example: 'COLD'
        name:
          description: Наименование объекта
          type: string
          example: 'Водозабор'
        address:
          description: Адрес объекта
          type: string
          example: 'Большой пр., д. 32'
        incidentsCount:
          description: Количество иницидентов на объекте в интевале date1-date2
          type: integer
          example: 2
        breakdownsCount:
          description: Количество аварий на объекте в интевале date1-date2
          type: integer
          example: 4
        inspectionsCount:
          description: Количество проверок объекта в интевале date1-date2
          type: integer
          example: 40
        violationsCount:
          description: Количество обнаруженных нарушений на объекте в интевале date1-date2
          type: integer
          example: 3
        checkStatus:
          type: string
          description: Статус проверки объекта
          enum: ["IS_GOOD", "IS_BAD", "NOT_CHECKED"]
          example: "IS_GOOD"
        developmentStatus:
          type: string
          description: Рабочий режим объекта
          enum: ["IN_OPERATION", "UNDER_REPAIR", "RESERVED", "NOT_OPERATED", "PLANNED"]
          example: "IN_OPERATION"
        commissioningDate:
          type: string
          description: Дата ввода в эксплуатацию
          format: date-time
          example: "2001-01-21T16:10:00Z"
        physicalWear:
          description: Физический износ объекта
          type: number
          example: 0.04,
        ratedPerformance:
          description: Мощность объекта номинальная
          type: number
          example: 220
        actualPerformance:
          description: Мощность объекта текущая
          type: number
          example: 200

    ReportsData:
      description: Массив отчетов
      type: array
      items:
        type: object
        properties:
          id:
            description: Идентификатор отчета
            type: integer
            example: 2
          status:
            description: Решение специалиста
            type: string
            enum: ['APPROVED', 'DECLINED', 'IN_PROGRESS', 'DATA_REQUIRED']
            example: 'APPROVED'
          date:
            type: string
            description: Дата создания (последнего изменения) отчета
            format: date-time
            example: "2021-02-09T12:00:00Z"
          comment:
            type: string
            description: Комментарий специалиста
            example: "Проверил. Одобряю"
